import React from 'react';

const ProductPrice = (props) => {
    return (
        <div>
            {(props.bestPrice != "R$ 0,00" && props.bestPrice != props.listPrice) && <div className="list-price">{props.bestPrice}</div>}
            <div className="best-price">{props.listPrice}</div>
        </div>
    );
};

export default ProductPrice;