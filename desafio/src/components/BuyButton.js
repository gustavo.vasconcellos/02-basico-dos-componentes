import React from 'react';

const BuyButton = (props) => {
    return (
        <div onClick={() => alert(`Você adicionou ao carrinho o produto: ${props.name} por ${props.price}`)}>
            Comprar
        </div>
    );
};

export default BuyButton;