import React from 'react';

const ProductImage = (props) => {
    return (
        <div className="image-block">
            <img src={props.image} />
        </div>
    );
};

export default ProductImage;