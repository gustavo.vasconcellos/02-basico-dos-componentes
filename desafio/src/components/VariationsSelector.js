import React, { Component } from 'react';

class VariationsSelector extends Component {
    constructor(props) {
        super(props)

        this.state = {
            dimensionSelected: 'P'
        }
    }

    handleClick(dimensionSelected, dimension) {
        this.setState({ dimensionSelected }, () => {
            const sku = this.props.skus.find(sku => sku.dimensions[dimension] === dimensionSelected).sku
    
            this.props.selectSku(sku)
        })
    }

    render() {
        const { dimensions, dimensionsMap } = this.props

        return (
            <div>
                {
                    dimensions.map(dimension => {
                        return (
                            <div>
                                <p>{dimension}</p>
                                <ul>
                                    {dimensionsMap[dimension].map(item => {
                                        return (
                                            <li 
                                                className={`${this.state.dimensionSelected === item ? 'selected' : ''}`}
                                                onClick={() => this.handleClick(item, dimension)}>{item}</li>
                                        )
                                    })}
                                </ul>
                            </div>
                        )
                    })
                }
            </div>
        );
    }
}

export default VariationsSelector;