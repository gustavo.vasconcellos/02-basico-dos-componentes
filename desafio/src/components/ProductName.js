import React from 'react';

const ProductName = (props) => {
    return (
        <div>
            <h1>{props.name}</h1>
        </div>
    );
};

export default ProductName;