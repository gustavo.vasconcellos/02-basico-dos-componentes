import React, { Component } from 'react'

import ProductImage from './components/ProductImage'
import ProductName from './components/ProductName'
import VariationsSelector from './components/VariationsSelector'
import ProductPrice from './components/ProductPrice'
import BuyButton from './components/BuyButton'

class ProductPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedSkuId: 0
        }

        this.setSelectedSkuId.bind(this)
    }

    componentDidMount() {
        this.setState({ selectedSkuId: this.props.product.skus[0].sku })
    }

    setSelectedSkuId(skuId) {
        this.setState({ selectedSkuId: skuId })
    }

    render() {
        const { selectedSkuId } = this.state
        const { product } = this.props

        const sku = product.skus.find(item => item.sku === selectedSkuId)
        console.log(this)
        return selectedSkuId !== 0 ? (
            <div>
                <div className="half-page">
                    <ProductImage image={sku.image} />
                </div>
                <div className="half-page">
                    <div className="name-block">
                        <ProductName name={sku.skuname} />
                    </div>
                    <div className="selector-block">
                        <VariationsSelector dimensions={product.dimensions} dimensionsMap={product.dimensionsMap} skus={product.skus} selectSku={this.setSelectedSkuId.bind(this)} />
                    </div>
                    <div className="price-block">
                        <ProductPrice bestPrice={sku.bestPriceFormated} listPrice={sku.listPriceFormated} />
                    </div>
                    <div className="buy-block">
                        <BuyButton name={sku.name} price={sku.bestPrice ? sku.bestPriceFormated : sku.listPriceFormated} />
                    </div>
                </div>
            </div>
        ) : (
                <div className="loading-block">
                    Carregando
            </div>
            )
    }
}

export default ProductPage